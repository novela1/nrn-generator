use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_yaml;
use std::env;
use std::fs;
use std::path::Path;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct NRNStruct {
    name: String,

    #[serde(default = "Option::default")]
    children: Option<Vec<Self>>,
    #[serde(default = "bool::default")]
    is_named: bool,
}

struct ChildInfo {
    content: String,
    name: String,
    offset: usize,
}

fn read_file(file_name: &str, offset_size: usize) -> Vec<String> {
    let file_ref_regex = Regex::new(r##"\#\!\[\[([A-Za-z\/_-]*\.yaml)\]\]"##).unwrap();

    let offset = " ".repeat(offset_size);

    let mut result = vec![];
    let content = fs::read_to_string(file_name).unwrap();
    for line_str in content.lines() {
        match file_ref_regex.captures(line_str) {
            Some(capture) => {
                let inject_start = capture.get(0).unwrap().start();
                result.push(offset.to_string() + &line_str[..inject_start]);
                let file_ref = capture.get(1).unwrap();
                let mut file_red_content = read_file(file_ref.as_str(), inject_start);
                result.append(&mut file_red_content);
            }
            None => result.push(offset.to_string() + line_str),
        }
    }
    result
}

fn capitalize_first_letter(s: &str) -> String {
    s[0..1].to_uppercase() + &s[1..]
}

fn generate_code(info: &NRNStruct, target_offset: usize) -> ChildInfo {
    let mut offset = target_offset.into();
    let name = format!(
        "Nrn{}{}",
        capitalize_first_letter(info.name.as_str()),
        offset
    );
    let mut result = format!(
        r##"#[derive(Debug, PartialEq)]
        pub struct {name} {{ path: String }}
        "##,
        name = name.to_string()
    )
    .to_owned();
    let mut methods = "".to_string().to_owned();
    //    methods.push_str("\npub fn get(self) -> String {self.path}\n");
    if info.is_named {
        methods.push_str(format!(r##"fn new<S: Into<String>>(path:S, resource_name:S) -> {name}  {{
                {name}{{path: format!("{{}}/{{}}/{{}}", path.into().as_str(), "{field_name}", resource_name.into())}}
            }}
            "##, name = name.to_string(), field_name = info.name
        ).as_str())
    } else {
        methods.push_str(
            format!(
                r##"fn new<S: Into<String>>(path:S) -> {name}  {{
                {name}{{path: format!("{{}}/{{}}", path.into().as_str(), "{field_name}")}}
            }}
            "##,
                name = name.to_string(),
                field_name = info.name
            )
            .as_str(),
        )
    };

    if let Some(children) = &info.children {
        methods.push_str("\npub fn any(self) ->String {format!(\"{}*\", self.path)}\n");
        for child in children {
            let ChildInfo {
                content,
                name: child_name,
                offset: new_offset,
            } = generate_code(&child, offset + 1);
            let child_call = if child.is_named {
                format!(
                    r##"
                    pub fn named_{ch_name}<S: Into<String>>(self, name: S) -> {child_name} {{
                        {child_name}::new(self.path.to_string(), name.into())
                    }}
                    pub fn any_{ch_name}(self) -> {child_name} {{
                        {child_name}::new(self.path.to_string(), "*".to_string())
                    }}
                "##,
                    ch_name = child.name
                )
            } else {
                format!(
                    r##"pub fn {ch_name}(self) -> {child_name} {{
                        {child_name}::new(self.path.to_string())
                    }}
            "##,
                    ch_name = child.name
                )
            };
            methods.push_str(child_call.as_str());
            result.push_str(content.as_str());
            offset = new_offset
        }
    }
    result.push_str(
        format!(
            "\nimpl {name} {{\n{methods}}}\n",
            name = name.to_string(),
            methods = methods
        )
        .as_str(),
    );
    result.push_str(
        format!(
            "\nimpl NRNChunk for {} {{fn get(&self) -> String {{self.path.to_string()}} }}\n",
            name.to_string()
        )
        .as_str(),
    );
    ChildInfo {
        content: result,
        name,
        offset: offset + 1,
    }
}

fn main() {
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("nrn.rs");

    let file = read_file("schema/nrn.yaml", 0);
    let yaml_file = file.join("\n");
    let parsed_file: NRNStruct = serde_yaml::from_str(yaml_file.as_str()).unwrap();
    let ChildInfo {
        content: mut generated_code,
        name: _,
        offset: _,
    } = generate_code(&parsed_file, 0);

    generated_code.push_str("\n\n pub fn nrn() -> NrnNrn0 { NrnNrn0::new(\"\") }");
    generated_code.push_str(
        r#"
pub trait NRNChunk
        where Self: Sized, {
    fn get(&self) -> String;
}
    "#,
    );

    fs::write(&dest_path, generated_code).unwrap();
}

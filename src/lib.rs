include!(concat!(env!("OUT_DIR"), "/nrn.rs"));
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum PermissionAction {
    None,
    Read,
    Write,
    Create,
    Delete,
}

impl Default for PermissionAction {
    fn default() -> Self {
        PermissionAction::None
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {

        println!("{:?}", nrn().any_user().invite().get());
        println!("{}", nrn().named_user("sdsd").any());
        println!("{}", nrn().named_role("cot").get());
        println!("{}", nrn().any_role().get());
        println!("{}", nrn().any());
        println!("{}", nrn().any());
        assert_eq!("/nrn/user/*/invite", nrn().any_user().invite().get());
    }
}
